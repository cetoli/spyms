#! /usr/bin/env python
# -*- coding: UTF8 -*-
# This file is part of  program SuperPython
# Copyright © 2020  Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__; `GPL <http://is.gd/3Udt>`__.
# SPDX-License-Identifier: (GPLv3-or-later AND LGPL-2.0-only) WITH bison-exception

"""Generate dynamic site from client side.

    This module controls the creation of the IDE user interface.

Classes in this module:

    :py:class:`Generate` Generate dynamic site from client side.

    :py:class:`CONST` Default file names for methods and other default constants.

Changelog
---------
    *Upcoming*
        * TODO: Improve documentation and test.
        * CHANGE: New Architecture.
    20.03
        * BETTER: Document classes and methods.
        * CHANGE: Move consts to a class.
        * NEW: Use Brython templates.
        * FIX: Working burger menus.
    19.10
        * NEW: Initial Implementation.

.. seealso::

   Page :ref:`core_introduction`

"""
import urllib.request


class Main:
    UI = None

    def __init__(self, browser, url=None):
        def create_tag(src, tag='script', _type="text/javascript"):
            """ Create a tag added to head"""
            _fp = urllib.request.urlopen(src)
            _tag = self.dc.createElement(tag)
            _tag.type, _tag.text = _type, _fp.read()
            self.dc.get(tag='head')[0].appendChild(_tag)

        def create_style_tag(href, _type="text/css", tag='style'):
            create_tag(f"css/{href}", _type=_type, tag=tag)

        self.url = url
        self.by = by = Main.UI = browser
        self.panel = by.document["_nav_panel_"]
        self.dc, self.ht, self.tp = by.document, by.html, by.template.Template
        [create_style_tag(href) for href in CONST.CSS.split()]
        self.action = ActionRenderProject(Responder())

    def render_main(self):
        """ Activate Brython templates and set events to burger menus.

        """

        def power_nav_burger():
            document = self.dc

            def toggle_burger(_element):
                _element = _element.target
                _target = document[_element.getAttribute('data-target')]
                # Toggle the class on both the "navbar-burger" and the "navbar-menu"
                _element.classList.toggle('is-active')
                _target.classList.toggle('is-active')

            nav_burger = document.select('.navbar-burger')
            for element in nav_burger:
                element.setAttribute("data-target", "navbar-menu")
                element.addEventListener('click', toggle_burger)

        self.tp("nav").render(alt="SuperPython")
        self.tp("footer").render(**CONST.FOOTER)
        # self.tp("apps", [navigate, navigate]).render(apps=CONST.APP)
        power_nav_burger()
        # self.dc["navbar-home"].bind("click", self._render_main)
        # self.dc["navbar-home"].bind("click", self.action.action_main())
        self.dc["navbar-home"].bind("click", self.action.action)
        self.dc["_navbar_start_"].bind("click", self.action.action)
        # self._render_main()
        # self.action.action_main(None)
        self.action.action()


class Responder:

    def __init__(self, panel=None, by=None, action=None):
        by = by or Main.UI
        self.dc, self.ht, self.tp = by.document, by.html, by.template.Template
        self.panel = panel or self.dc["_nav_panel_"]
        self.action = action if action else lambda *_: None

    def render_page(self, head, name, content):
        """ Show a folder with current level items.
        obj_id = ev.target.id.split("_<ap>_")[1] if ev else None
        name, content, *_ = CONST.APP[obj_id] if obj_id else CONST.HEAD
        """
        self.panel.html = ""  # ""
        self.dc["_main_header_"].html = head
        self.dc["_page_title_"].html = name
        self.dc["_page_motto_"].html = content
        self.panel.html = ""  # ""

    def _render_item(self, formatter, template, name, obj_id, col=2, render=None):
        """ Show an icon representing an item.
        """
        render = render or self.render_page
        obj_id = obj_id or name
        out = template.format(**formatter)
        div = self.ht.DIV(Class=f"column is-{col}")
        div.html = out
        _ = self.panel <= div
        self.dc[f"dc_<ap>_{obj_id}"].bind("click", render)

    def render(self, name=None, content=None, obj_id=None, img=None, idx=0):
        """ Show an item icon.

        """
        _ = self, idx
        obj_id = obj_id or name
        self._render_item(
            formatter=dict(obj_id=obj_id, alt=name, mot=content, img=img),
            template=CONST.PROJECT,
            name=name,
            obj_id=obj_id or name,
            col=4,
            render=self.action
        )


class ResponderModule(Responder):
    def render(self, name=None, content=None, obj_id=None, img=None, idx=0):
        """ Show an item icon.

        """
        _ = self, img
        img = 'garden.jpg'
        obj_id = obj_id or name
        self._render_item(
            formatter=dict(
                fig_top='{}px'.format(-200 * (idx // 6)),
                fig_left='{}px'.format(-200 * (idx % 6)),
                obj_id=obj_id,
                alt=name,
                mot=content[:30],
                img=img

            ),
            template=CONST.FOLDER,
            name=name,
            obj_id=obj_id or name,
            col=2,
            render=self.action
        )


class ResponderItem(Responder):
    def render(self, name=None, content=None, obj_id=None, img=None, idx=0):
        """ Show an item icon.

        """
        _ = self, img
        img = 'miro.jpg'
        obj_id = obj_id or name
        self._render_item(
            formatter=dict(
                fig_top='{}px'.format(-200 * (idx // 6)),
                fig_left='{}px'.format(-200 * (idx % 6)),
                obj_id=obj_id,
                alt=name,
                mot=content[:30],
                img=img
            ),
            template=CONST.FOLDER,
            name=name,
            obj_id=obj_id or name,
            col=2,
            render=self.action
        )


class ResponderEdit(Responder):
    def render(self, name=None, content=None, obj_id=None, img=None, idx=0):
        """ Show an item icon.

        """
        _ = self, img
        self.dc["_main_header_"].html = ""
        self.panel.html = CONST.EDITOR


class Action:
    def __init__(self, responder, model=None):
        self.model = model or CONST.BASE
        responder.action = self.action
        self.responder = responder

    def action(self, ev=None):
        # obj_id = ev.target.id.split("_<ap>_")[1] if ev else "main"
        oid = ev.target.id if ev else ""
        obj_id = oid.split("_<ap>_")[1] if "_<ap>_" in oid else None
        self.model = CONST.BASE[obj_id] if obj_id else CONST.APP
        name, content, *_ = CONST.APP[obj_id] if obj_id else CONST.HEAD
        head = CONST.HEADER
        target = self.responder
        target.render_page(head, name, content)
        # print("self.model.values()", self.model.values())
        self._action(target)

    def _action(self, target):
        [Item(*params, idx=idx).render(target) for idx, params in enumerate(self.model.values())]


class ActionRenderItem(Action):
    def _action(self, target):
        self.responder.action = Action(ResponderEdit()).action
        [Item(*params, idx=idx).render(target) for idx, params in enumerate(self.model.values())]


class ActionRenderModule(Action):
    def _action(self, target):
        self.responder.action = ActionRenderItem(ResponderItem()).action
        [Item(*params, idx=idx).render(target) for idx, params in enumerate(self.model.values())]


class ActionRenderProject(Action):
    def _action(self, target):
        self.responder.action = ActionRenderModule(ResponderModule()).action
        [Item(*params, idx=idx).render(target) for idx, params in enumerate(self.model.values())]


class Item:
    def __init__(self, name, content, obj_id, img, idx=0):
        self.name, self.content, self.obj_id, self.img, self.idx = name, content, obj_id, img, idx

    def render(self, target):
        target.render(self.name, self.content, self.obj_id, self.img, idx=self.idx)

    def update(self, name=None, content=None, obj_id=None, img=None, idx=None):
        self.name, self.content = name or self.name, content or self.content
        self.obj_id, self.img, self.idx = obj_id or self.obj_id, img or self.img, idx or self.idx


class CONST:
    HEAD = ["SUPER PYTHON", "Learn python with games", "", "image/supygirls_logo.png"]
    HEADER = """
    <section class="hero">
        <div class="hero-body">
            <div class="container">
                <div class="has-text-centered">
                    <!-- header && subheader -->
                    <h1 id="_page_title_" class="title is-1 is-spaced">SUPER PYTHON</h1>
                    <h4 id="_page_motto_" class="subtitle is-4">learn python with games</h4>
                    <!-- end of header && subheader -->

                </div>
            </div>
        </div>
    </section>
    """
    CSS = "atom-one-light.css  codemirror.css roboto.css solarized-light.css " \
          "bulma.css font-awesome.min.css solarized.css style.css"
    APP = dict(
        SuperPython=["SuperPython", "Gather your team to develop your own game", '', "image/camisasuperpython.png"],
        Kwarwp=["Kwarwp", "Follow the adventures of a brave warrior", "", "image/abertura_kwarwpp.jpg"],
        SuPyGirls=["SuPyGirls", "Roll your own game with python", "", "image/supygirls_logo.png"],
    )
    BASE = dict(
        SuperPython=dict(
            SuperPython=["SuperPython", "Gather your team to develop your own game", '', "image/camisasuperpython.png"],
            Kwarwp=["Kwarwp", "Follow the adventures of a brave warrior", "", "image/abertura_kwarwpp.jpg"],
            SuPyGirls=["SuPyGirls", "Roll your own game with python", "", "image/supygirls_logo.png"],
        ),
        Kwarwp=dict(
            SuperPython=["SuperPython", "Gather your team to develop your own game", '', "image/camisasuperpython.png"],
            Kwarwp=["Kwarwp", "Follow the adventures of a brave warrior", "", "image/abertura_kwarwpp.jpg"],
            SuPyGirls=["SuPyGirls", "Roll your own game with python", "", "image/supygirls_logo.png"],
        ),
        SuPyGirls=dict(
            SuperPython=["SuperPython", "Gather your team to develop your own game", '', "image/camisasuperpython.png"],
            Kwarwp=["Kwarwp", "Follow the adventures of a brave warrior", "", "image/abertura_kwarwpp.jpg"],
            SuPyGirls=["SuPyGirls", "Roll your own game with python", "", "image/supygirls_logo.png"],
        ),
    )
    FOOTER = dict(
        footer=[
            ["http://www.ufrj.br",
             "image/ufrj-logo-8.png", "UFRJ"],
            ["http://www.nce.ufrj.br",
             "image/nce-logo-8.png", "NCE"],
            ["http://labase.nce.ufrj.br",
             "image/labase-logo-8.png", "LABASE"],
            ["http://www.sbc.org.br/2-uncategorised/1939-programa-superpython",
             "image/sbc-logo-8.png", "SBC"], ],
        link="http://www.superpython.net",
        loading="www.superpython.net"
    )
    FOLDER = """
    <!-- start of post -->
        <div id="dc_<ap>_{obj_id}" class="card">
            <!-- image for post -->
            <div id="di_<ap>_{obj_id}" class="card is-2by2" style="height:114px; overflow:hidden;">
                <figure>
                    <a href="#">
                        <img id="hi_<ap>_{obj_id}" src="image/{img}" width="1000px" alt="{alt}"
                             style="position:relative; min-width:1200px;
                top:{fig_top};
               left:{fig_left};"></a>
                </figure>
            </div>
            <!-- end of image for post -->

            <!-- post header -->
            <div id="dv_<ap>_{obj_id}" class="card-content-header">
                <h6  id="dh_<ap>_{obj_id}" class="title is-6">{alt}</h6>
            </div>
            <!-- end of post header -->
            <font size='1'>{mot}</font>

            <!-- post content -->
        </div>
    <!-- end of post -->
    """
    PROJECT = """
                <div id="dc_<ap>_{obj_id}" class="card">
                    <!-- image for post -->
                    <div id="di_<ap>_{obj_id}" class="card-image">
                        <figure class="image is-4by3">
                                <img id="hi_<ap>_{obj_id}" src="{img}" alt="{alt}">
                        </figure>
                    </div>
                    <!-- end of image for post -->

                    <!-- post header -->
                    <div id="dv_<ap>_{obj_id}" class="card-content-header">
                        <h4 id="dh_<ap>_{obj_id}" class="title is-4"><a id="hl_<ap>_{alt}" href="#">{alt}</a></h4>
                    </div>
                    <!-- end of post header -->
                    {mot}

                    <!-- post content -->
                </div>
    """
    EDITOR = """
            <!-- start of about -->
            <div class="column is-12">
                <div class="card">
                    <!-- about content -->
                    <div id="pycard"></div>
                    <div id="pydiv" class="card is-12by8" style="min-height:600px;">
                        <figure>
                            <img src="image/camisasuperpython.png" width="1000px" alt="SuperPython">
                        </figure>
                    </div>
                </div>
            </div>
            <!-- end of about column -->

    """
    MENU = """
        <a class="navbar-item" href="http://www.superpython.net">
          <img src="image/camisasuperpython.png" alt="Superpython" width="60" height="28">
        </a>
        <span class="navbar-burger" data-target="navbar-menu">
            <span></span>
            <span></span>
            <span></span>
        </span>
    """
    MENUBAR = """
        <!-- this "nav-menu" is hidden on mobile -->
        <div id="right-menu" class="navbar-end">
            <a class="navbar-item is-tab" href="info/help.html">
                Help
            </a>
            <a class="navbar-item is-tab" href="info/about.html">
                About
            </a>
            <a class="navbar-item is-tab" href="/">
                Home
            </a>
        </div>
        <!-- end of nav -->
    """
    MODEL = dict(
        main=APP,
        project=BASE
    )
