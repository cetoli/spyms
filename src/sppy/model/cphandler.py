#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Este arquivo é parte do programa Sppy
# Copyright 2011 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__;
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em `GPL <http://j.mp/GNU_GPL3>`__.
""" Handles request to Gitlab.
"""
__author__ = "Carlo E T Oliveira"
import json
from urllib.parse import urlencode, quote_plus
import os as op
from urllib.request import urlopen, Request

try:
    TOKEN = op.environ["GLSPPY"]
except KeyError:
    TOKEN = "nonono"

TK = f"?private_token={TOKEN}"
GITLAB = "https://gitlab.com/api/v4/"
USR = "cetoli"


class Handler:
    """ Handles request to Gitlab.
    """
    API = dict(
        create_project=f"{GITLAB}projects/user/{{user_id}}{TK}",
        get_projects=f"{GITLAB}users/{USR}/projects{TK}&simple=true",
        get_code=f"{GITLAB}projects/{{repo_id}}/repository/files/{{file_path}}{TK}&ref={{branch}}",
        create_code=f"{GITLAB}projects/{{repo_id}}/repository/files/{{file_path}}{TK}",
        save_code=f"{GITLAB}projects/{{repo_id}}/repository/files/{{file_path}}{TK}",
        get_modules=f"{GITLAB}projects/{{repo_id}}/repository/tree{TK}"
    )

    def __init__(self):
        self.api = Handler.API

    def _urlopen(self, request):
        _res = urlopen(request).read()
        _j_res = json.loads(_res.decode('utf-8'))
        if isinstance(_j_res, dict):
            self.jo = self._create_json_object(**_j_res)
        if isinstance(_j_res, list):
            self.jo = [self._create_json_object(**part) for part in _j_res]
        return _res

    def _url_open(self, api_call='get_projects', data=None, method=None, **kwargs):
        method = method or "POST" if data else "GET"
        _url = self.api[api_call].format(**kwargs)
        _req = Request(_url, data=data, method=method)
        print(f"url_open:{api_call}", "POST" if data else "GET", _url, _req.data)
        # return urlopen(_url, data=data).read()
        _res = self._urlopen(_req)
        return _res

    def _create_json_object(self, **kwargs):
        class JsonObject:
            def __init__(self, **kwargs_):
                [setattr(self, name, value) for name, value in kwargs_.items()]
        return JsonObject(**kwargs)

    def get_projects(self):
        """
        Obtain all projects for a user in repository.

        :return: JSON string with project representation.
        """
        return self._url_open()

    def get_code(self, module, path="", branch="master"):
        """
        Obtain a given code file for a user in repository.

        :param module: module of required code.
        :param path: name path of required code.
        :param branch: branch of the code, defaults to *master*
        :return: JSON string with project representation.
        """
        _path = quote_plus(path)
        return self._url_open(api_call='get_code', repo_id=module, file_path=_path, branch=branch)

    def get_modules(self, module):
        """
        Obtain a list of modules for a given project and user in repository.

        :param module: module of required code.
        :return: JSON string with project representation.
        """
        _module = quote_plus(module, safe='')
        return self._url_open(api_call='get_modules', repo_id=module)

    def create_project(self, user_id, project_name):
        """
        Create a given project for a user in repository.

        :param user_id: username to project creation.
        :param project_name: name of project to be created.
        :return: JSON string with project representation.
        """
        _project_name = urlencode({"name": project_name})
        return self._url_open(api_call='create_project', data=_project_name, user_id=user_id)

    def create_code(self, project_name, code_name, code):
        """
        Create a given code file for a user in repository.

        :param project_name: name of project where code will be created.
        :param code_name: name of code file to be created.
        :param code: code contents of the file to be created.
        :return: JSON string with project representation.
        """
        _data = {
            "branch": "master", "author_email": "author@example.com", "author_name": "Firstname Lastname",
            "content": code, "commit_message": f"create a new file: {code}"
        }
        _data_ = urlencode(_data)
        _code_name = quote_plus(code_name, safe='')
        _project_name = quote_plus(project_name, safe='')
        return self._url_open(api_call='create_code', data=_data_, repo_id=_project_name, file_path=code_name)

    def save_code(self, project_name, code_name, code):
        """
        Update a given code file for a user in repository.

        :param project_name: name of project where code will be created.
        :param code_name: name of code file to be created.
        :param code: code contents of the file to be created.
        :return: JSON string with project representation.
        """
        _data = {
            "branch": "master", "author_email": "author@example.com", "author_name": "Firstname Lastname",
            "content": code, "commit_message": f"create a new file: {code}"
        }
        _data_ = urlencode(_data)
        _code_name = quote_plus(code_name, safe='')
        _project_name = quote_plus(project_name, safe='')
        return self._url_open(api_call='save_code', data=_data_,
                              repo_id=_project_name, file_path=code_name, method="PUT")
