#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Este arquivo é parte do programa Sppy
# Copyright 2011 Carlo Oliveira <carlo@nce.ufrj.br>,
# `Labase <http://labase.selfip.org/>`__;
#
# Você deve ter recebido uma cópia da Licença Pública Geral GNU
# junto com este programa, se não, veja em `GPL <http://j.mp/GNU_GPL3>`__.
""" Define the structure of a development repository.
"""
__author__ = "Carlo E T Oliveira"


def _represent(value):
    if isinstance(value, (str, int, float, list, tuple, dict)):
        return value
    else:
        return repr(value)


class Serializable(dict):
    """"""

    def __init__(self, *args, **kwargs):
        """
        Permite a criação como um objeto JSON.

        :param args: parametros consecutivos
        :param kwargs: parâmetros de dicionário
        """
        super().__init__(*args, **kwargs)
        # hack to fix _json.so make_encoder serialize properly
        self.__setitem__('dummy', 1)

    def _my_attributes(self):
        """
        Gera atributos automaticamente.

        :return: lista dos atributos coletados.
        """
        return [
            (x, _represent(getattr(self, x)))
            for x in self.__dir__()
            if x not in Serializable().__dir__()
        ]

    def __repr__(self):
        return '<%s.%s object at %s>' % (
            self.__class__.__module__,
            self.__class__.__name__,
            hex(id(self))
        )

    def items(self):
        return ((x, y) for x, y in self._my_attributes())

    def keys(self):
        return iter([x[0] for x in self._my_attributes()])

    def values(self):
        return iter([x[1] for x in self._my_attributes()])


class Part(Serializable):
    """ generic Part"""
    def __init__(self, name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._name = name


class User(Part):
    """
        Repository User

        :param name: O nome desta parte.

    """
    def __init__(self, name):
        """

        :param name: O nome desta parte.
        """
        super().__init__(name)


class Project(Part):
    """
        Repository Project

        :param name: O nome deste projeto.

    """

    def __init__(self, name):
        super().__init__(name)


class Module(Part):
    """
        Repository Module

        :param name: O nome deste módulo.
        :param project: O nome deste projeto.

    """
    def __init__(self, name, project=None):
        super().__init__(name)
        self._project = project


class Code(Module):
    """
        Repository Code

        :param name: O nome deste código.
        :param module: O nome deste módulo.
        :param project: O nome deste projeto.
        :param contents: O conteúdo deste código.

    """

    def __init__(self, name, module=None, project=None, contents=None):
        super().__init__(name, project=project)
        self._module = module
        self._contents = contents


if __name__ == "__main__":
    import json
    code = Code(*"file mod project contents".split())
    print(json.dumps(code))
