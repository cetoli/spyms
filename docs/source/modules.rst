.. _spyms_modules:

################################
SpyMS - Core Modules
################################

.. _core_main:

Main Module
=================

.. automodule:: core.main
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Web
    :synopsis: Create the IDE UI dynamically.

.. seealso::

   Page :ref:`core_introduction`

.. note::
   Main core modules.

