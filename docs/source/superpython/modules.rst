.. _modulos_superpython:

################################
SuperPython - Módulos Principais
################################

Módulo Cliente
==============

.. automodule:: sppy.model.structure
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Web
    :synopsis: Modelo representando um repositório.


.. seealso::

   Module :mod:`sppy.model.cphandler`

.. note::
   Unidade de Modelo Cliente, modelo principal.


Gitlab Handler
==============

.. automodule:: sppy.model.cphandler
    :members:
    :undoc-members:
    :show-inheritance:
    :platform: Web
    :synopsis:  Interface de acesso a um repositório..


.. seealso::

   Module :mod:`sppy.model.structure`

.. note::
   Unidade de Modelo Cliente.

