.. SuperPython documentation master file, created by
   sphinx-quickstart on Sat Oct 12 08:58:41 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. |superpython| image:: _static/superpython.png
   :target: superpython/index.html
   :width: 200pt
   :height: 100pt

Welcome to SuperPython's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro.rst
   modules.rst
   changes.rst

+----------------+
| |superpython|  |
+----------------+
| SuperPython    |
+----------------+

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
