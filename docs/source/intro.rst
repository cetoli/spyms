.. _core_introduction:

SPyMS - Manual
==================

    SuperPython is a WEB IDE designed to teach Python. It is specially target to teach children in early scholar age.

    The Microservice implementation allows it to run from a static server. All logic runs inside the browser.
    Persistence is done saving all code with REST calls to web enabled git.

Generating main page
--------------------
    his module controls the creation of the IDE user interface.

    .. seealso::

       Page :ref:`core_main`


SpyMS - Modules
===================

Investigate content semantics studying graphs extracted from texts

Documented functionality:

* SpyMS Core Modules : Basic Entities

    :ref:`spyms_modules`

