Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_.


`Unreleased`_
-------------

Added
+++++
- Changelog file.
- Use Brython Templates

Fixed
+++++
- Working burger menus.

`19.10`_
--------

Fixed
+++++
- indexer and utilities CLI execution.

Added
+++++
- First version with gitlab model, test and docs


-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**


.. ..

 [LABASE](http://labase.sefip.org) -
 [Instituto Tércio Pacitti](http://nce.ufrj.br) -
 [UFRJ](http://www.ufrj.br)

 ![LABASE Laboratório de Automação de sistemas Educacionais][logo]

 [logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"
 <!---

.. ..

LABASE_ - `Instituto Tércio Pacitti`_ - UFRJ_

.. image:: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png
   :alt: Laboratório de Automação de sistemas Educacionais




.. _LABASE: http://labase.nce.ufrj.br/
.. _Instituto Tércio Pacitti: http://nce.ufrj.org/
.. _ufrj: http://www.UFRJ.BR/
.. _Unreleased: https://gitlab.com/cetoli/spyms/-/commit/375b4f8103e868dc8a78086de56cc3de6d9cf681
.. _19.10: https://gitlab.com/cetoli/spyms/-/commit/375b4f8103e868dc8a78086de56cc3de6d9cf681
.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/

.. ..

 --->

.. ..