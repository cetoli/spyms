import base64
import unittest
from urllib.parse import quote_plus

from urllib.request import Request

from sppy.model.structure import Code
from sppy.model.cphandler import Handler
import json


class MockHandlerTestCase(unittest.TestCase):
    """
    Test local python handling requests to Gitlab server. Uses mock to avoid effective sendind.
    """

    def setUp(self) -> None:
        self.cd = Code(*"file mod project contents".split())
        self.ch = Handler()
        self.api = Handler.API
        self.ch._urlopen = self._urlopen
        self.cd_js = '{"_name": "file", "_project": "project", "_module": "mod", "_contents": "contents"}'

    def _urlopen(self, request):
        return request

    def test_handler_create_code(self):
        """ create a new code file"""
        module = "cetoli/sppy19"
        result = self.ch.create_code(module, "__author__.py", "carlo")
        obj = result
        # print(len(obj), obj[0]['name'], obj)
        print("test_handler_create_code", obj.full_url, obj.data, dir(obj))
        self.assertIn("carlo", result.data, result.data)
        self.assertEqual("POST", result.method)

    def test_handler_save_code(self):
        """ save and update a code file"""
        module = "cetoli/sppy19"
        result = self.ch.save_code(module, "__author__.py", "carlo")
        obj = result
        # print(len(obj), obj[0]['name'], obj)
        print("test_handler_save_code", obj.full_url, obj.data, dir(obj))
        self.assertIn("carlo", result.data, result.data)
        self.assertEqual("PUT", result.method)

    def test_handler_create_project(self):
        """ create a new project"""
        user = quote_plus("cetoli", safe='')
        project = quote_plus("sppy19", safe='')
        result = self.ch.create_project(user, project)
        obj = result
        # print(len(obj), obj[0]['name'], obj)
        print("test_handler_create_project", obj.full_url, obj.data, dir(obj))
        self.assertIn(user, result.full_url, f"{result.full_url}, user:{user}")
        self.assertIn(project, result.data, f"{result.data}, project:{project}")
        self.assertEqual("POST", result.method)


class MyTestCase(unittest.TestCase):
    """
    Test local python handling requests to Gitlab server. Retrieve real Gitlab data.
    """
    def setUp(self) -> None:
        self.cd = Code(*"file mod project contents".split())
        self.ch = Handler()
        self.cd_js = '{"_name": "file", "_project": "project", "_module": "mod", "_contents": "contents"}'

    def test_json_conversion(self):
        """ Get json string converted to python dict"""
        self.assertEqual(self.cd_js, json.dumps(self.cd))

    def test_handler_project_list(self):
        """ Retrieve a list of projects for the user in Gitlab."""
        result = self.ch.get_projects().decode('utf-8')
        obj = json.loads(result)
        jobj = self.ch.jo
        prnames = [pr.name for pr in jobj]
        print("test_handler_project_list", jobj[0].name, len(obj), obj[0]['name'], obj, dir(jobj[0]))
        print("test_handler_project_list", prnames)
        self.assertIn("crescent", result, result)
        self.assertIn("EIDOLOOM", prnames, prnames)

    def test_handler_code(self):
        """ Retrieve a repository file for the user in Gitlab."""
        # result = self.ch.get_module("_spy").decode('utf-8')
        # module = "8335208"
        module = "cetoli%2F_spy"
        result = self.ch.get_code(module, "__author__.py").decode('utf-8')
        obj = json.loads(result)
        # print(len(obj), obj[0]['name'], obj)
        print(len(obj), self.ch.jo.content, obj)
        print("self.ch.jo.content", base64.b64decode(self.ch.jo.content))
        self.assertIn("__author__.py", result, result)

    def test_handler_modules(self):
        """ Retrieve a list of modules for the user in Gitlab."""
        # result = self.ch.get_module("_spy").decode('utf-8')
        module = "8335208"
        # module = "cetoli%2F_spy"
        result = self.ch.get_modules(module).decode('utf-8')
        obj = json.loads(result)
        jobj = self.ch.jo
        prnames = [pr.name for pr in jobj]
        print("test_handler_modules", prnames)
        # print(len(obj), obj[0]['name'], obj)
        print(len(obj), obj)
        self.assertIn("vitollino", result, result)
        self.assertIn("tesouro", prnames, prnames)


if __name__ == '__main__':
    unittest.main()
