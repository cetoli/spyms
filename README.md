# SPYMS
Superpython IDE implemented with microservices.

    Author: Carlo E. T. Oliveira, Cibele R. C. Oliveira
    Affiliation: Universidade Federal do Rio de Janeiro
    Email: carlo at ufrj br
    
## SuperPython Section


SuperPython is a WEB IDE designed to teach Python. It is specially target to teach children in early scholar age.

The Microservice implementation allows it to run from a static server. All logic runs inside the browser.
Persistence is done saving all code with REST calls to web enabled git.

-------

Laboratório de Automação de Sistemas Educacionais
-------------------------------------------------

**Copyright © Carlo Olivera**

[LABASE](http://labase.sefip.org) - 
[Instituto Tércio Pacitti](http://nce.ufrj.br) - 
[UFRJ](http://www.ufrj.br)

![LABASE Laboratório de Automação de sistemas Educacionais][logo]

[logo]: https://cetoli.gitlab.io/spyms/image/labase-logo-8.png "Laboratório de Automação de sistemas Educacionais"

